
Configuration
=============

If you have already configured the mongodb module to connect to your mongodb 
instance, this module requires no further configuration.

New Installation
=============

Simply enable the module and your spaces_overrides data will be stored in
mongodb.  If you have an existing site with existing spaces_overrides data, see
the section on Migration.

Migration
=============

If you have an existing site which currently uses MySQL to store the
spaces_overrides table, you will need to take your site offline, enable the
module and migrate the spaces_overrides data manually.  Example commands to
export the existing data and import it are shown below:

Export existing MySQL data:

  SELECT type, id, object_type, object_id, value 
    INTO OUTFILE '/tmp/spaces_overrides.csv'
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n' 
    FROM spaces_overrides; 
  
Import data into MongoDB: 

NOTE: drupal is the default database for mongodb - if you are using a 
non-default database, be sure to change this value

IMPORTANT: this command will drop the spaces_overrides collection if it already
exists in the drupal database.

  mongoimport -d drupal -c spaces_overrides -type csv -f type,id,object_type,object_id,value --drop /tmp/spaces_overrides.csv
